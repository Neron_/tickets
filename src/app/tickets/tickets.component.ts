import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit {

  searchName = '';
  name = '';

  constructor(private http: HttpClient) {}

  items = [];

  ngOnInit() {

    this.http.get<TicketsComponent[]>('./assets/tickets.json').subscribe(data => {
      this.items = data
    });
  }

}
