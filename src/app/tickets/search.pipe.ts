import { Pipe, PipeTransform } from '@angular/core';
import { TicketsComponent } from './tickets.component';

@Pipe({
  name: 'search'
})

export class SearchPipe implements PipeTransform {
  // transform(items,value) {
  //   return items.filter(item => {
  //     return item.name.includes(value);
  //   });
  // }

  transform(items: TicketsComponent[], searchTerm: string): TicketsComponent[] {
    if (!items || !searchTerm) {
        return items;
    }

    return items.filter(item =>
        item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1);
}

}