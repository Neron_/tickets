import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule }   from '@angular/common/http';

// import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TicketsComponent } from './tickets/tickets.component';
import { TicketComponent } from './ticket/ticket.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoadticketsService } from './loadtickets.service';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { SearchPipe } from './tickets/search.pipe';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'ticket/:id', component: TicketComponent},
  {path: 'tickets', component: TicketsComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    TicketsComponent,
    TicketComponent,
    HomeComponent,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgbModule.forRoot(),
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [LoadticketsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
