import { TestBed } from '@angular/core/testing';

import { LoadticketsService } from './loadtickets.service';

describe('LoadticketsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoadticketsService = TestBed.get(LoadticketsService);
    expect(service).toBeTruthy();
  });
});
