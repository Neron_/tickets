import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit {

  constructor(private ar: ActivatedRoute, private http: HttpClient) {
    ar.params.subscribe(param => {
      console.log(param);
      this.param = param;
    });
  }

  param;
  id = {};
  description = {};
  item = {};
  // item = {
  //   description: ''
  // };


  ngOnInit() {
    const param = this.param;
    this.http.get<TicketComponent[]>('./assets/tickets.json').subscribe(data => {
    this.item = data.find(i => i.id === parseInt(param.id, 10));
    });

    
  }



}